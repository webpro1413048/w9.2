import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/services/auth'
import { useMessageStore } from '@/stores/message'
import { useRoute, useRouter } from 'vue-router'
import { useLoadingStore } from './loading'

export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()
  const router = useRouter()
  const loading = useLoadingStore()

  const login = async (email: string, password: string) => {
    loading.doLoad()
    try {
      const res = await authService.login(email, password)
      console.log(res.data)
      messageStore.showMessage('Login Success')
      localStorage.setItem('user', JSON.stringify(res.data.user))
      localStorage.setItem('access_token', res.data.access_token)
      router.push('/')
    } catch (e: any) {
      console.log(e)
      messageStore.showMessage(e)
    }
    loading.finish()
  }

  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    // console.log(JSON.parse(strUser))
    return JSON.parse(strUser)
  }

  function getToken(): string | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken === null) return null
    return JSON.parse(strToken)
  }

  function logout() {
    router.replace('/login')

    localStorage.removeItem('user')
    localStorage.removeItem('access_token')
  }
  return { getCurrentUser, getToken, login, logout }
})
