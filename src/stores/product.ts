import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import productService from '@/services/product'
import type { Role } from '@/types/Role'
import { useMessageStore } from './message'
import type { Product } from '@/types/Product'

export const useProductStore = defineStore('product', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const products = ref<Product[]>([])
  const roles = ref<Role[]>([])
  const initialProduct: Product & { files: File[] } = {
    name: '',
    price: 0,
    type: { id: 2, name: 'drink' },
    image: 'noImage.jpg',
    files: []
  }

  const editedProduct = ref<Product & { files: File[] }>(JSON.parse(JSON.stringify(initialProduct)))

  async function getProduct(id: number) {
    try {
      loadingStore.doLoad()
      const res = await productService.getProduct(id)
      editedProduct.value = res.data
      loadingStore.finish()
    } catch (error: any) {
      messageStore.showMessage(error.message)
      loadingStore.finish()
    }
  }

  async function getProducts() {
    loadingStore.doLoad()
    const res = await productService.getProducts()
    products.value = res.data
    loadingStore.finish()
  }

  async function saveProduct() {
    try {
      /////////////// try catch to defind problem
      loadingStore.doLoad()
      ///////////
      console.log(editedProduct.value)
      const product = editedProduct.value
      if (!product.id) {
        // Add new
        console.log('Post ' + JSON.stringify(product))
        const res = await productService.addProduct(product)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(product))
        const res = await productService.updateProduct(product)
      }

      await getProducts()
      loadingStore.finish()
    } catch (error: any) {
      messageStore.showMessage(error.message) //show error message
      loadingStore.finish()
    }
  }
  async function deleteProduct() {
    try {
      loadingStore.doLoad()
      const product = editedProduct.value
      const res = await productService.delProduct(product)

      await getProducts()
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
    }
  }

  //getType
  async function getType() {
    const res = await productService.getType()
    roles.value = res.data
  }

  function clearForm() {
    editedProduct.value = JSON.parse(JSON.stringify(initialProduct))
  }
  return {
    products,
    getProducts,
    saveProduct,
    deleteProduct,
    editedProduct,
    getProduct,
    clearForm,
    getType,
    roles
  }
})
